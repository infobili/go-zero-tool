package cmd

import (
	"bufio"
	"fmt"
	"github.com/spf13/cobra"
	"go/ast"
	"go/parser"
	"go/token"
	"io"
	"io/fs"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
)

var dir string

var responseDir string

var pkg string

func init() {
	rebuildTypesCommand.PersistentFlags().StringVar(&dir, "dir", "",
		"types files directory")
	rootCmd.AddCommand(rebuildTypesCommand)

	rebuildLogicsCommand.PersistentFlags().StringVar(&dir, "dir", "",
		"logic files directory")
	rootCmd.AddCommand(rebuildLogicsCommand)

	AddJsonRespCommand.PersistentFlags().StringVar(&dir, "dir", "",
		"handler files directory")
	AddJsonRespCommand.PersistentFlags().StringVar(&pkg, "pkg", "",
		"json response package path")
	AddJsonRespCommand.PersistentFlags().StringVar(&responseDir, "resp-dir", "",
		"json response file directory")
	rootCmd.AddCommand(AddJsonRespCommand)
}

var validateFunc = `func (req *%s) Validate() error {
	return validate.Struct(req)
}
`

var validateFileStr = `package types

import "github.com/go-playground/validator/v10"

var validate *validator.Validate

func init() {
	validate = validator.New()
}
`

var rebuildTypesCommand = &cobra.Command{
	Use: "rebuild-types",
	Run: func(cmd *cobra.Command, args []string) {
		if dir == "" {
			log.Fatal("types files directory can not be empty")
		}

		goFileName := strings.TrimRight(dir, getDirectorySeparator()) + getDirectorySeparator() + "types.go"
		do(goFileName)

		validateFilePath := dir + getDirectorySeparator() + "validate.go"
		if !isExist(validateFilePath) {
			err := os.WriteFile(validateFilePath, []byte(validateFileStr), fs.ModePerm)
			if err != nil {
				log.Fatalf("failed to write %s: %+v\n", validateFilePath, err)
			}
		}

		_ = execCommand("gofmt", "-w", validateFilePath)
	},
}

var respFileStr = fmt.Sprintf(`package response

import (
	"fmt"
	"github.com/zeromicro/go-zero/rest/httpx"
	"net/http"
)

type response struct {
	Code int         %sjson:"code"%s
	Msg  string      %sjson:"msg"%s
	Data interface{} %sjson:"data,omitempty"%s
}

func JsonResp(w http.ResponseWriter, msg string, code int, data interface{}) {
	if msg == "" {
		v, ok := codeMsgs[code]
		if ok {
			msg = v
		}
	}

	httpx.OkJson(w, response{
		Msg:  msg,
		Code: code,
		Data: data,
	})
}

func MergeCodeMsgsMapping(mmp map[int]string) {
	for key, msg := range mmp {
		if v, ok := codeMsgs[key]; ok {
			panic(fmt.Sprintf("error code is repeat, please check ! code: %d, exist message: %s", key, v))
		}
		if key <= 1 {
			panic("error code must be greater than 1 !")
		}
		codeMsgs[key] = msg
	}
}

// 定义自己的返回code
const (
	ServerException int = iota - 1
	Success
	ParamsError
	RecordNotFound
	TokenInvalid
	UserDisabled
	PermissionDeny
	FrequencyLimit
)

var codeMsgs = map[int]string{
	ServerException: "Server Error",
	Success:         "Success",
	ParamsError:     "参数错误！",
	RecordNotFound:  "记录不存在！",
	TokenInvalid:    "Token失效！",
	UserDisabled:    "你已被禁用！",
	PermissionDeny:  "你没有此操作权限！",
	FrequencyLimit:  "频率限制！",
}
`, "`", "`", "`", "`", "`", "`")

var AddJsonRespCommand = &cobra.Command{
	Use: "add-json-resp",
	Run: func(cmd *cobra.Command, args []string) {
		if dir == "" {
			log.Fatal("handler file directory can not be empty")
		}

		if pkg == "" {
			log.Fatal("json response package path can not be empty")
		}

		if responseDir == "" {
			log.Fatal("json response file directory can not be empty")
		}

		respFileName := strings.TrimRight(responseDir, getDirectorySeparator()) + getDirectorySeparator() + "response.go"

		if !isExist(respFileName) {
			basePath := filepath.Dir(respFileName)
			// 文件夹不存在则生成
			basePathExist := isExist(basePath)
			if !basePathExist {
				// 递归创建文件夹
				err := os.MkdirAll(basePath, os.ModePerm)
				if err != nil {
					log.Fatalf("create folder: %s recursively failed! error: "+err.Error(), basePath)
				}
			}

			err := os.WriteFile(respFileName, []byte(respFileStr), fs.ModePerm)
			if err != nil {
				log.Fatalf("failed to write %s: %+v\n", respFileName, err)
			}

			_ = execCommand("gofmt", "-w", respFileName)
		}

		log.Println("walk handler files")
		err := filepath.WalkDir(dir, visit)
		if err != nil {
			log.Fatalf("rebuild handler files WalkDir error: %v", err)
		}

		for _, file := range needRebuildGoFiles {
			if strings.Contains(file, "resp.go") || strings.Contains(file, "routes.go") {
				continue
			}
			b, err := readAll(file)
			if err != nil {
				log.Fatalf("read handler file %s error: %v", file, err)
				return
			}
			str := string(b)

			if strings.Contains(str, pkg) {
				continue
			}

			str = strings.ReplaceAll(str, "jsonResp", "response.JsonResp")
			str = strings.ReplaceAll(str, "import (", "import (\n\t\""+pkg+"\"")
			str = strings.ReplaceAll(str, "paramsError", "response.ParamsError")
			str = strings.ReplaceAll(str, "success", "response.Success")

			err = rewriteFile(file, str)
			if err != nil {
				log.Fatalf("rebuild logic file: %s:  failed! error: %v", file, err.Error())
				return
			}

			_ = execCommand("gofmt", "-w", file)
		}
	},
}

var needRebuildGoFiles []string

var logicCodeMsgsMappingTemplate = `// const ()

// var codeMsgsMapping = map[int]string{}

// func init() {
//     response.MergeCodeMsgsMapping(codeMsgsMapping)
// }`

var rebuildLogicsCommand = &cobra.Command{
	Use: "rebuild-logics",
	Run: func(cmd *cobra.Command, args []string) {
		if dir == "" {
			log.Fatal("logic files directory can not be empty")
		}

		log.Println("walk logic files")
		err := filepath.WalkDir(dir, visit)
		if err != nil {
			log.Fatalf("rebuild logic files WalkDir error: %v", err)
		}

		for _, file := range needRebuildGoFiles {
			b, err := readAll(file)
			if err != nil {
				log.Fatalf("read logic file %s error: %v", file, err)
				return
			}
			str := string(b)
			if !strings.Contains(str, "response.MergeCodeMsgsMapping") {
				if tmpLastIndex := strings.LastIndex(file, "/"); tmpLastIndex != -1 {
					fileName := file[tmpLastIndex+1:]
					if tmpIndex := strings.Index(fileName, "_logic"); tmpIndex != -1 {
						logicCodeMsgsMapping := strings.ReplaceAll(logicCodeMsgsMappingTemplate, "codeMsgsMapping", underscoreToCamelCase(fileName[:tmpIndex])+"CodeMsgsMapping")
						str = strings.Replace(str, ")", ")\n\n"+logicCodeMsgsMapping, 1)
					}
				}

				if strings.Contains(str, ") error {") {
					str = strings.ReplaceAll(str, ") error {", ") (msg string, code int) {")
					str = strings.ReplaceAll(str, "return nil", "return")
				}

				str = strings.ReplaceAll(str, ", err error) {", ", msg string, code int) {")
			}

			err = rewriteFile(file, str)
			if err != nil {
				log.Fatalf("rebuild logic file: %s:  failed! error: %v", file, err.Error())
				return
			}

			_ = execCommand("gofmt", "-w", file)
		}

		log.Printf("rebuild logic file successed!")
	},
}

func visit(path string, info os.DirEntry, err error) error {
	if err != nil {
		log.Fatalf("traverse files error: %v", err)
	}
	if !info.IsDir() {
		needRebuildGoFiles = append(needRebuildGoFiles, path)
	}
	return nil
}

func do(fileName string) {
	b, err := readAll(fileName)
	if err != nil {
		log.Fatalf("read types file %s error: %v", fileName, err)
		return
	}
	str := string(b)

	// 解析源代码文件
	file, err := parser.ParseFile(token.NewFileSet(), "", str, parser.AllErrors)
	if err != nil {
		log.Fatalf("parse types file, %s error: %v", fileName, err)
		return
	}

	structs := make([]string, 0)

	hasValidate := false
	// 遍历文件中的所有声明
	for _, decl := range file.Decls {
		// 判断是否为类型声明语句
		if genDecl, ok := decl.(*ast.GenDecl); ok && genDecl.Tok == token.TYPE {
			// 遍历类型声明中的所有类型
			for _, spec := range genDecl.Specs {
				if typeSpec, ok := spec.(*ast.TypeSpec); ok {
					// 判断是否为结构体类型
					if typeSpec.Type.(*ast.StructType) != nil {
						//fmt.Println(typeSpec.Name.Name, "xxx")
						prefix := "type " + str[typeSpec.Pos()-1:typeSpec.End()]
						suffix := ""
						if strings.Contains(prefix, "validate:") {
							hasValidate = true
							suffix = fmt.Sprintf(validateFunc, typeSpec.Name.Name) + fmt.Sprintf(`var _ validation.Validator = (*%s)(nil)`, typeSpec.Name.Name)
						}
						currnetStruct := prefix + suffix
						structs = append(structs, currnetStruct)
					}
				}
			}
		}
	}

	imports, err := parser.ParseFile(token.NewFileSet(), "", str, parser.ImportsOnly)
	importsStr := str[imports.Pos()-1 : imports.End()]
	if hasValidate {
		importsStr += `
import "github.com/zeromicro/go-zero/core/validation"`
	}

	err = rewriteFile(fileName, importsStr+"\n"+strings.Join(structs, "\n"))
	if err != nil {
		log.Fatalf("rebuild types file: %s:  failed! error: %v", fileName, err.Error())
		return
	}
	_ = execCommand("gofmt", "-w", fileName)

	log.Printf("rebuild types file successed! file: %s", fileName)
}

type GOOS string

func (g GOOS) String() string {
	return string(g)
}

const (
	Windows GOOS = "windows"
	Linux   GOOS = "linux"
	Darwin  GOOS = "darwin"
	FreeBSD GOOS = "freebsd"
)

// getGOOS 获取系统信息
func getGOOS() GOOS {
	if runtime.GOOS == "windows" {
		return Windows
	}
	if runtime.GOOS == "darwin" {
		return Darwin
	}
	if runtime.GOOS == "freebsd" {
		return FreeBSD
	}
	return Linux
}

func getDirectorySeparator() string {
	if getGOOS() == Windows {
		return "\\"
	}

	return "/"
}

func readAll(filePth string) ([]byte, error) {
	f, err := os.Open(filePth)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return io.ReadAll(f)
}

func execCommand(name string, arg ...string) error {
	_, err := exec.Command(name, arg...).CombinedOutput()
	if err != nil {
		return err
	}

	return nil
}

func rewriteFile(path, str string) error {
	// 打开一个存在的文件，将原来的内容覆盖掉
	// O_WRONLY: 只写, O_TRUNC: 清空文件
	file, err := os.OpenFile(path, os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}

	defer file.Close() // 关闭文件
	// 带缓冲区的*Writer
	writer := bufio.NewWriter(file)
	_, err = writer.WriteString(str)
	if err != nil {
		return err
	}

	// 将缓冲区中的内容写入到文件里
	err = writer.Flush()
	if err != nil {
		return err
	}

	return nil
}

func isExist(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		if os.IsNotExist(err) {
			return false
		}
		return false
	}
	return true
}

func underscoreToCamelCase(s string) string {
	words := strings.Split(s, "_")

	// 将第一个单词小写
	result := words[0]

	// 将后续单词首字母大写拼接
	for i := 1; i < len(words); i++ {
		result += strings.Title(words[i])
	}

	return result
}
